# Create your views here.
from __future__ import unicode_literals
import io

from django.shortcuts import render, redirect
import matplotlib
from matplotlib.font_manager import FontProperties

matplotlib.use('SVG')

from pylab import *

rcParams['figure.figsize'] = 5, 2

from pierwsza.models import Poll, Question, Choice


def index(request):
    # three lists- Poll objects,done polls, todo_poll_list
    poll_list = list(Poll.objects.all().order_by("pub_date"))
    done_poll_list = []
    todo_poll_list = []

    done_poll_id_list = request.session.get('saved_id', [])

    # for every poll in poll list, check if it is done or to do

    for poll in poll_list:
        # checking if poll id is saved in session
        if str(poll.id) in done_poll_id_list:
            done_poll_list.append(poll)
        else:
            todo_poll_list.append(poll)

    return render(request, "index.html", {'done_poll_list': done_poll_list,
                                          'todo_poll_list': todo_poll_list})


def poll_description_view(request, poll_id):
    questions = list(Question.objects.filter(poll=poll_id).order_by('number'))
    list_question_id = [question.id for question in questions]
    question_id = list_question_id[0]

    description_list = list(Description.objects.filter(poll=poll_id))
    description = [description.description for description in description_list]

    return render(request, "description.html", {'poll_id': poll_id,
                                                'description': description,
                                                'question_id': question_id})


def question_view(request, poll_id, question_id):
    question_id = int(question_id)

    if request.method == 'GET':

        return get_question_view(request, question_id, poll_id)

    elif request.method == 'POST':
        return seleted_choice_view(request, poll_id, question_id)


def get_question_view(request, question_id, poll_id):
    # if choice is saved in session- I want to get it
    # if not I get an empty list

    saved_choice = request.session.get('selected_list', [])
    saved_id = [int(choice_id) for choice_id in saved_choice]

    id_list = request.session.get('saved_id', [])
    question = Question.objects.get(id=question_id)
    choice_list = list(Choice.objects.filter(ques=question))

    return render(request, "question.html", {'question': question,
                                             'poll_id': poll_id,
                                             'choice_list': choice_list,
                                             'saved_id': saved_id,
                                             'id_list': id_list})


def missing_choice_view(request, question_id, poll_id):
    question = Question.objects.get(id=question_id)
    choice_list = Choice.objects.filter(ques=question_id)

    return render(request, "question.html", {'question': question,
                                             'poll_id': poll_id,
                                             'choice_list': choice_list,
                                             'bad_choice': True})


def going_to_next_question(request, poll_id, question_id, selected_choice):
    # button next question
    question = Question.objects.get(id=question_id)  # getting all the question objects with id
    question_list = list(Question.objects.filter(poll=poll_id).order_by(
        'number'))  # making a list of Questions filtered by id and ordered by question number

    current_question_index = question_list.index(question)  # getting index of current question
    if selected_choice.next_question is None:  # if choice doesn't have next question field set up
        return no_selected_choice(request, current_question_index, question_list, poll_id)

    else:
        # if choice has next question field set up
        next_question = selected_choice.next_question

        return redirect('/pierwsza/' + poll_id + '/' + str(next_question.id))


def get_previous_choice_id(request, question_id):
    list_choice_objects = list(Choice.objects.filter(ques=question_id))
    list_choice = [choice.id for choice in list_choice_objects]  # listę id choiców do danego pytania

    list_of_choice_id = request.session.get('selected_list', [])
    zapisane_choice_id = [int(wybor_id) for wybor_id in list_of_choice_id]  # lista id choiców zapisanych w sesji

    for session_id in list_choice:  # dla każdego id w liście id choiców do danego pytania
        if session_id in zapisane_choice_id:  # jeśli id jest na liscie id choiców zapisanych w sesji
            return session_id

    return None


def save_to_session(request, selected_choice):
    selected_choice_list = request.session.get('selected_list', [])
    selected_choice_list.append(selected_choice.id)
    request.session['selected_list'] = selected_choice_list


def add_vote(request, selected_choice):
    selected_choice.votes += 1
    selected_choice.save()
    save_to_session(request, selected_choice)


def finding_previous_choice(request):
    list_of_choice_id = request.session.get('selected_list', [])
    zapisane_choice_id = [int(wybor_id) for wybor_id in list_of_choice_id]
    previous_choice_id = zapisane_choice_id[-1]
    return previous_choice_id


def remove_from_session(request, choice_id):
    selected_choice_list = request.session.get('selected_list', [])
    selected_choice_list.remove(choice_id)
    request.session['selected_list'] = selected_choice_list


def decrease_vote(request, previous_choice_id):
    previous_choice = Choice.objects.get(id=previous_choice_id)

    previous_choice.votes -= 1
    previous_choice.save()
    remove_from_session(request, previous_choice_id)


def update_vote(request, selected_choice, question_id):
    previous_choice_id = get_previous_choice_id(request, question_id)
    decrease_vote(request, previous_choice_id)
    add_vote(request, selected_choice)


def saving_votes(selected_choice_id, request, question_id, poll_id):
    selected_choice = Choice.objects.get(id=selected_choice_id)
    previous_choice = get_previous_choice_id(request, question_id)

    if previous_choice is None:
        add_vote(request, selected_choice)
    elif previous_choice == int(selected_choice_id):
        pass
    else:
        update_vote(request, selected_choice, question_id)

    return going_to_next_question(request, poll_id, question_id, selected_choice)


def seleted_choice_view(request, poll_id, question_id):
    selected_choice_id = request.POST.get("choice_id", None)

    if selected_choice_id is not None:

        return saving_votes(selected_choice_id, request, question_id, poll_id)

    elif selected_choice_id is None:

        return missing_choice_view(request, question_id, poll_id)


def no_selected_choice(request, current_question_index, question_list, poll_id):
    if current_question_index < len(question_list) - 1:
        next_question = question_list[current_question_index + 1]
        # go to the next question
        return redirect('/pierwsza/' + poll_id + '/' + str(next_question.id))
    else:
        # after finishing the poll I store in cookie id of finished poll

        id_list = request.session.get('saved_id', [])
        id_list.append(poll_id)
        request.session['saved_id'] = id_list

        return redirect('/pierwsza/last_page.html')


def last_page(request):
    information = "To już ostatnie pytanie, dziekujemy za wypelnienie ankiety!"

    back = "Wróć do strony głównej"

    return render(request, "last_page.html", {'information': information,
                                              'back': back})


def default_poll_view(request, poll_id):
    # widok ma kierowac na pytanie pierwsze w ankiecie,
    # ktore ma jakis id, ale w danej ankiecie jest ustawione jako pierwsze

    # wyciagam pytanie pierwsze z danej ankiety
    questions = Question.objects.filter(poll=poll_id).order_by('number')
    first_question = questions[0]

    return question_view(request, poll_id, first_question.id)


def finding_percent_list(ques, vote_list):
    percent_list = []
    for vote in vote_list:
        percent = int(round(vote / sum(vote_list) * 100))
        percent_list.append(percent)
    ques.percent_list = percent_list


def creating_vote_list(choices):
    vote_list = []  # list of votes for choices of current question
    for choice in choices:
        vote_list.append(choice.votes)
    return vote_list


def getting_poll_title(poll_id):
    last_poll_list = list(Poll.objects.filter(id=poll_id))
    last_poll = last_poll_list[0]
    poll_title = last_poll.title
    return poll_title


def creating_plots(axis_choices, ques, vote_list):
    matplotlib.rcParams['svg.fonttype'] = 'none'

    font_param = FontProperties()
    font_param.set_family('Arial')
    font_param.set_size(11)

    y_pos = np.arange(len(axis_choices)) + .5
    x_pos = vote_list
    plt.barh(y_pos, x_pos, xerr=0.5, align='center', height=0.5, alpha=0.5, color='b')
    plt.yticks(y_pos, axis_choices, fontproperties=font_param)
    plt.subplots_adjust(left=0.4)

    imgdata = io.StringIO()
    plt.savefig(imgdata)
    plt.clf()
    buf = imgdata.getvalue()
    ques.svg_graph = buf
    imgdata.close()


def results_view(request):
    poll_id_list = request.session.get('saved_id', [])
    poll_id = poll_id_list[-1]

    poll_title = getting_poll_title(poll_id)

    question_1 = Question.objects.filter(poll=poll_id).order_by('number')

    for ques in question_1:
        choices = list(Choice.objects.filter(ques=ques))
        ques.choice_list = choices

        axis_choices = list(Choice.objects.filter(ques=ques))  # list of choices for plots

        vote_list = creating_vote_list(choices)

        finding_percent_list(ques, vote_list)

        vote_list.reverse()
        axis_choices.reverse()

        creating_plots(axis_choices, ques, vote_list)

    return render(request, 'results.html', {'question_1': question_1,
                                            'poll_title': poll_title, })
