from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',

                       url(r'^$', 'pierwsza.views.index', name='index'),
                       url(r'^pierwsza/(?P<poll_id>\d+)$', 'pierwsza.views.default_poll_view'),
                       url(r'^pierwsza/(?P<poll_id>\d+)/(?P<question_id>\d+)$', 'pierwsza.views.question_view'),
                       url(r'^pierwsza/last_page.html$', 'pierwsza.views.last_page'),
                       url(r'^pierwsza/results.html$', 'pierwsza.views.results_view'),
                       url(r'^admin/', include(admin.site.urls)),
)




